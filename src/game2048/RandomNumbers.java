package game2048;

import java.util.Random;

public class RandomNumbers {

	private int index1, index2;

	Random random1 = new Random();
	Random random2 = new Random();
	
    Random random = new Random();
    int randomNumber = random.nextInt(100);
    int generateNumber = (randomNumber < 10) ? 4 : 2;



	public void doItRandom() {
		index1 = random1.nextInt(4);
		index2 = random2.nextInt(4);
		generateNumber = (randomNumber < 10) ? 4 : 2;

		
	}

	public int getGenerateNumber() {
		return generateNumber;
	}

	public void setGenerateNumber(int generateNumber) {
		this.generateNumber = generateNumber;
	}

	public int getIndex1() {
		return index1;
	}

	public void setIndex1(int index1) {
		this.index1 = index1;
	}

	public int getIndex2() {
		return index2;
	}

	public void setIndex2(int index2) {
		this.index2 = index2;
	}

	public Random getRandom1() {
		return random1;
	}

	public void setRandom1(Random random1) {
		this.random1 = random1;
	}

	public Random getRandom2() {
		return random2;
	}

	public void setRandom2(Random random2) {
		this.random2 = random2;
	}

}
