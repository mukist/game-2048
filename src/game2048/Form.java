package game2048;

public class Form {

	RandomNumbers rn = new RandomNumbers();

	public Integer[][] array = new Integer[4][4];

	public void printInitialForm() {
		rn.doItRandom();
		
		array[rn.getIndex1()][rn.getIndex2()] = rn.getGenerateNumber();
		while (true) {
			if (isCellNull()) {
				array[rn.getIndex1()][rn.getIndex2()] = rn.getGenerateNumber();
				break;

			} else {
				rn.doItRandom();
			}
		}

		screenTable();
	}

	public void generateOneCell() {
		rn.doItRandom();		
		while (true) {
			if (isCellNull()) {
				array[rn.getIndex1()][rn.getIndex2()] = rn.getGenerateNumber();
				break;
			} else {
				rn.doItRandom();
			}
		}

	}

	private boolean isCellNull() {
		return array[rn.getIndex1()][rn.getIndex2()] == null;
	}

	public void screenTable() {

		for (int i = 0; i < 4; i++) {
			System.out.print("|");
			for (int j = 0; j < 4; j++) {
				System.out.printf("%4s", array[i][j] != null ? array[i][j] : "");
				System.out.print("|");
			}
			System.out.println("");
			System.out.println("_____________________");

		}
	}

}
