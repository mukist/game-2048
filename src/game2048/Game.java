package game2048;

import java.util.Scanner;

public class Game {

	Scanner scan = new Scanner(System.in);
	Movement move = new Movement();
	Form form = new Form();

	public void start() {
		System.out.println(
				"Welcome to the 2048. Press the up(w) down(s) left(a) right(d) keys.");
		System.out.println();
		form.printInitialForm();

		boolean isGameContinue = true;
		while (isGameContinue) {
			String choosenMove = scan.nextLine();
			choosenMove = choosenMove.toUpperCase();

			switch (choosenMove) {
		
			case "S": 
				move.moveDown(form);
				break;

			case "W": 
				move.moveUp(form);
				break;
				
			case "A": 
				move.moveLeft(form);
				break;
				
			case "D": 
				move.moveRight(form);
				break;
			
			default:
				System.out.println("Please press the up(w) down(s) left(a) right(d) keys. Unexpected value: " + choosenMove);
			}
		}

	}

}
