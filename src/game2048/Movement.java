package game2048;

import java.util.ArrayList;
import java.util.List;

public class Movement {

	public boolean moveDown(Form form) {
		fillDown(form);
		form.generateOneCell();
		form.screenTable();
		return true;

	}

	public boolean moveUp(Form form) {
		fillUp(form);
		form.generateOneCell();
		form.screenTable();
		return true;

	}

	public boolean moveLeft(Form form) {
		fillLeft(form);
		form.generateOneCell();
		form.screenTable();
		return true;
	}

	public boolean moveRight(Form form) {
		fillRight(form);
		form.generateOneCell();
		form.screenTable();
		return true;
	}

	public void fillUp(Form form) {
		List<Integer> mergeControl = new ArrayList<>();

		for (int i = 0; i < 4; i++) {
			for (int j = 1; j < 4; j++) {
				if (form.array[j][i] != null) {
					int copy = j - 1;

					while (copy >= 0) {
						if (form.array[copy][i] == null) {
							form.array[copy][i] = form.array[copy + 1][i];
							form.array[copy + 1][i] = null;
						} else if (form.array[copy][i].equals(form.array[copy + 1][i])
								&& !mergeControl.contains(copy)) {
							form.array[copy][i] *= 2;
							form.array[copy + 1][i] = null;
							mergeControl.add(copy);
							break;
						} else {
							break;
						}

						copy--;
					}
				}
			}

			mergeControl.clear();
		}
	}

	public void fillLeft(Form form) {
		List<Integer> mergeControl = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                if (form.array[i][j] != null) {
                    int copy = j - 1;

                    while (copy >= 0) {
                        if (form.array[i][copy] == null) {
                        	form.array[i][copy] = form.array[i][copy + 1];
                        	form.array[i][copy + 1] = null;
                        } else if (form.array[i][copy].equals(form.array[i][copy + 1]) && !mergeControl.contains(copy)) {
                        	form.array[i][copy] *= 2;
                        	form.array[i][copy + 1] = null;
                            mergeControl.add(copy);
                            break;
                        } else {
                            break;
                        }

                        copy--;
                    }
                }
            }

            mergeControl.clear();
        }
    }

	public void fillDown(Form form) {
		List<Integer> mergeControl = new ArrayList<>();

		for (int i = 0; i < 4; i++) {
			for (int j = 2; j >= 0; j--) {
				if (form.array[j][i] != null) {
					int copy = j + 1;

					while (copy < 4) {
						if (form.array[copy][i] == null) {
							form.array[copy][i] = form.array[copy - 1][i];
							form.array[copy - 1][i] = null;
						} else if (form.array[copy][i].equals(form.array[copy - 1][i])
								&& !mergeControl.contains(copy)) {
							form.array[copy][i] *= 2;
							form.array[copy - 1][i] = null;
							mergeControl.add(copy);
							break;
						} else {
							break;
						}

						copy++;
					}
				}
			}

			mergeControl.clear();
		}
	}

	public void fillRight(Form form) {
		List<Integer> mergeControl = new ArrayList<>();

		for (int i = 0; i < 4; i++) {
			for (int j = 2; j >= 0; j--) {
				if (form.array[i][j] != null) {
					int copy = j + 1;

					while (copy < 4) {
						if (form.array[i][copy] == null) {
							form.array[i][copy] = form.array[i][copy - 1];
							form.array[i][copy - 1] = null;
						} else if (form.array[i][copy].equals(form.array[i][copy - 1])
								&& !mergeControl.contains(copy)) {
							form.array[i][copy] *= 2;
							form.array[i][copy - 1] = null;
							mergeControl.add(copy);
							break;
						} else {
							break;
						}

						copy++;
					}
				}
			}

			mergeControl.clear();
		}
	}

}
